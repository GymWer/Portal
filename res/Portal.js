var icons;
iconOptionsIndex=-1;
var editmodeState=0;
var placeholder;
var dilinkTarget;
var firstExtension;
var calcResult;
var dilinkDomains;
var dilinkIndex;
var settingsArray;
var actualInpuut;
var borderline="";
var searchEngines;
var settingsArrayOld;

function importSettings(text){
	settingsArrayOld=settingsArray;
	document.getElementById("settingsText").value=text;
	settingsArray=JSON.parse(text);
settings=settingsArray[0];
icons=settingsArray[1];
actualiseIcons();
saveSettings();
}

function restoreDefaultSettings(){
setCookie("settings", null, 365);
getSettings();
}

function GET(theUrl)
  {
    var xmlHttp = null;
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false );
    xmlHttp.send( null );
    return xmlHttp.responseText;
  }

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}


function openUrl(URL){
    if(getSetting("linknewtab")==false){
        window.location.href=URL;
    }else{
        window.open(URL, "_blank");
    }
}

function getSetting(id){
    counter=0;
    success=0;
    var result;
    while(success==0){
    if(settings[counter][0]==id){
     result=settings[counter][1];
     success=1;
    }else if(counter==(settings.length-1)){
        success=-1;
    }
    counter++;
    }
     if(result==null){
      return null;
     }else{
         return result;
     }
}

function changeSetting(id, value){
    counter=0;
    success=0;
    while(success==0){
    if(settings[counter][0]==id){
     settings[counter][1]=value;
        success=1;
        //console.log("Settings changed");
    }
        if(counter==(settings.length-1)){
        success=-1;
    }
    counter++;
    }
     if(success==-1){
      settings.push([id, value]);                               
      //console.log("Settingspoint added");
     }
     saveSettings();
}

function search(e){
e.preventDefault();
//console.log("search(e)");
eval(firstExtension);
}

function submitInput(e){
e.preventDefault();
window[firstExtension]();
}

function inputChange(input){
    document.getElementById('inputSuggestions').innerHTML="";
    borderline="";
    firstExtension="";
    actualInput=input;
    dilink(input);
    calc(input);
    websearch(input);
    algebra(input);
    //weather(input);
}

function addInputSuggestion(icon, content, onclick){
document.getElementById('inputSuggestions').innerHTML+=`<tr>
<td>
<div class="text" style="` + borderline + `padding-top: 10px; margin-bottom: 5px; margin-left: 5px; margin-right: 5px; cursor: pointer; font-size: 15px" onclick="` + onclick + `"><img style="height:15px; margin-right: 10px;" src="` + icon + `"></img>` + content + `</div>
</td>
</tr>`;
borderline="border-top: 1px solid #4d4d4d;";
}

function weather(input){
	if (input.length>2&&/^[a-zA-Z]+$/.test(input)){
		if(input=="welden"){
            var dataRaw=GET("res/forecast.json");
        //var dataRaw=GET("http://api.openweathermap.org/data/2.5/forecast?q=" + input + "&APPID=3102aec5b849c81949f1fbbe0b56f692&units=metric");
	var data=JSON.parse(dataRaw);
	if(dataRaw!='{"cod":"404","message":"city not found"}'){
	//sleep(1000);
	if(input==actualInput){
	addInputSuggestion("extensions/weather/weather-"+ data.list[0].weather[0].main.toLowerCase() +  ".svg", data.list[0].main.temp + "<div style='height:auto; width:100%' class='weather-chart'>", "");
    labelsArray=[];
                for(var i=0; i<12; i++){
                    labelsArray.push(i);
                }
                
                dataArray=[];
                for(var i=0; i<12; i++){
                    dataArray.push({x: i, y: data.list[i].main.temp});
                }
                var chart = new Chartist.Line('.weather-chart', {
  labels: labelsArray,
  series: [
{
      data: dataArray
  }
      ]
},  {
  plugins: [
    Chartist.plugins.tooltip()
  ]
});
}
	}
	}
        }
}


function websearch(input){
    //console.log("WebSearch: " + input);
    addInputSuggestion("extensions/websearch/icon.svg", (input), "openUrl('https://www.qwant.com/?q=" + encodeURIComponent(input) + "&t=all');");
    if(firstExtension==""){
        firstExtension="openUrl('https://www.qwant.com/?q=" + encodeURIComponent(input) + "&t=all');"
    }
}

function websearchEvent(){
$.magnificPopup.open({
  items: {
    src: '<div id="searchW" class="white-popup mp" style="max-width: 100%; width: calc(100% - 40px); height: calc(100vh - 80px);"><iframe id="searchIframe" seamless style="margin-top: 20px; width:100%; height: calc(100% - 20px);"></iframe></div>', // can be a HTML string, jQuery object, or CSS selector
    type: 'inline'
  }
});
document.getElementById("searchIframe").src='https://www.qwant.com/?q=' + encodeURIComponent(document.getElementById('searchInput').value) + '&t=all';
    //window.location.href=('https://www.qwant.com/?q=' + encodeURIComponent(document.getElementById('searchInput').value) + '&t=all');
}

function agpl(){
document.getElementById("agpl").innerHTML=GET('res/agpl-3.0.html');
//console.log("agpl");
}
     
     $.getJSON("extensions/dilink/domains.json", function(data) {
    //console.log("dilinkDomains loaded from domains.json");
   dilinkDomains=data;
});
     
function dilink(input){
if(/^[a-z-]+$/.test(input)){
 var success=0;
 dilinkIndex=0;
    while(success<1){
        if(dilinkDomains[dilinkIndex][0]==input){
url=dilinkDomains[dilinkIndex][1];
url=url.replace(/https:\/\//g, "");
url=url.replace(/http:\/\//g, "");
url=url.replace(/www./g, "");
         addInputSuggestion(dilinkDomains[dilinkIndex][1] + "/favicon.ico", url, "openUrl('"+dilinkDomains[dilinkIndex][1]+"');");
            success++;
            if(firstExtension==""){
            firstExtension="openUrl('"+dilinkDomains[dilinkIndex][1]+"');";
            }
        }
        if(dilinkIndex==(dilinkDomains.length-1)){
         success=1;
        }
        dilinkIndex++;
    }
}
}

function dilinkEvent(){
    window.location.href=(dilinkTarget);
}

function calcReplace(input){
    input=input.replace(/pi/g, "Math.PI");
	input=input.replace(/round/g, "Math.round");
	input=input.replace(/root/g, "Math.sqrt");
	input=input.replace(/abs/g, "Math.abs");
	//input=input.replace(/ceil/g, "Math.ceil");
	input=input.replace(/floor/g, "Math.floor");
    	input=input.replace(/acos/g, "(180/Math.PI)*Math.aaccooss");
	input=input.replace(/asin/g, "(180/Math.PI)*Math.aassiinn");
	input=input.replace(/atan/g, "(180/Math.PI)*Math.aattaann");
    	input=input.replace(/cos\(/g, "Math.cos((Math.PI/180)*");
	input=input.replace(/sin\(/g, "Math.sin((Math.PI/180)*");
	input=input.replace(/tan\(/g, "Math.tan((Math.PI/180)*");
    input=input.replace(/Math.aaccooss/g, "Math.acos");
    input=input.replace(/Math.aassiinn/g, "Math.asin");
	input=input.replace(/Math.aattaann/g, "Math.atan");
	//input=input.replace(/min/g, "Math.min");
	//input=input.replace(/max/g, "Math.max");
	input=input.replace(/random/g, "Math.random()");
	input=input.replace(/e/g, "Math.E");
	input=input.replace(/,/g, ".");
    input=input.replace(/\[/g, "(");
    input=input.replace(/{/g, "(");
    input=input.replace(/]/g, ")");
    input=input.replace(/}/g, ")");
    //console.log(input);
    return input;
}

function calcTest(input){
    var errorState=0;
try {
    (function(){
        eval(calcReplace(input));
    })()
}
catch(e) {
    errorState=1;
}
if(/^[a-z]+$/.test(input)){
errorState=1;		
}
if(input=="pi" || input=="e"){
errorState=0;	
}
return errorState;
}

function algebra(input){
if (/^[0-9\/\*.,()erandomminmaxsintanfloorceilabstroundcospih+-]+$/.test(input)) {
calcTestResult=calcTest(input.replace(/x/g, "5"));                
input=calcReplace(input);
                algebraStart=-5;
                algebraEnd=5;
                algebraStepWidth=1;
                if(calcTestResult!=1 || input=="x"){
                addInputSuggestion("extensions/algebra/icon.svg", "<div style='margin-top: -20px; height:auto; width:auto' class='algebra-chart'>", "algebraEvent();");
                labelsArray=[];
                for(var i=algebraStart; i<=algebraEnd; i=i+algebraStepWidth){
                    labelsArray.push(i);
                }
                
                dataArray=[];
                for(var i=algebraStart; i<=algebraEnd; i=i+algebraStepWidth){
                    dataArray.push({x: i, y: eval(input.replace(/x/g, i))});
                }
                var chart = new Chartist.Line('.algebra-chart', {
  labels: labelsArray,
  series: [
{
      data: dataArray
  }
      ]
},  {
  plugins: [
    Chartist.plugins.tooltip()
  ]
});
}
}
}


function calc(input){
if (/^[0-9\/\*.,()rooterandomminmaxsintanfloorceilabstroundcospih+-]+$/.test(input)) {
input=input.replace(/x/g, "*");
	calcTestResult=calcTest(input);
input=calcReplace(input);
if(calcTestResult!=1 && input.indexOf("**") == -1){
calcResult=eval(input);
	if(calcResult.toString()!=document.getElementById("searchInput").value && !/[{]/.test(calcResult)){
    		html=document.getElementById('inputSuggestions').innerHTML;
    		addInputSuggestion("extensions/calc/icon.svg", (calcResult), "calcEvent()");
    		document.getElementById('inputSuggestions').innerHTML+=html;
     		firstExtension="calcEvent();";
	}
} 
}
}
function calcEvent(){
 document.getElementById("searchInput").value=calcResult;
 document.getElementById('inputSuggestions').innerHTML="";
}
 
function iconToLeft(index){
    if(index!=0){
        placeholder=icons[index];
        icons[index]=icons[index-1];
        icons[index-1]=placeholder;
        iconOptionsIndex=index-1;
        //console.log("Wird verschoben");
    }
    actualiseIcons();
}

function iconToRight(index){
    if(index<(icons.length-1)){
        placeholder=icons[index];
        icons[index]=icons[index+1];
        icons[index+1]=placeholder;
        iconOptionsIndex=index+1;
        //console.log("Wird verschoben");
    }
    actualiseIcons();
}
 
 function editmode(){
  if(editmodeState==0){
      editmodeState=1;
      document.getElementById("edit").src='res/check.svg';
      document.getElementById("edit").style.width='50px';
      document.getElementById("edit").style.height='50px';
}else{
    editmodeState=0;
    document.getElementById("edit").src='res/edit.svg';
    document.getElementById("edit").style.width='25px';
      document.getElementById("edit").style.height='25px';
}
    actualiseIcons();
}


function exportSettings(){
settingsArray=["Placeholder"];
    settingsArray[0]=settings;
    settingsArray.push(icons);
    var blob = new Blob([JSON.stringify(settingsArray)], {type: "text/plain;charset=utf-8"});
    saveAs(blob, "PortalSettings.json");
}

function handleFileSelect(evt) {
    var files = evt.target.files;

    f = files[0];

    var reader = new FileReader();

    reader.onload = (function(theFile) {
        return function(e) {
        	importSettings(e.target.result);
        };
      })(f);
      reader.readAsText(f);
  }
  
function actualiseIcons(){
    document.getElementById("icons").innerHTML="";

  if(editmodeState==1){
      icons.forEach(innerActualiseIconsEditmode);
      document.getElementById("icons").innerHTML+=`<a onclick="iconOptionsIndex=icons.length;" href="#iconOptionsW" class="open-popup-link"><img src="res/add.svg" class="ab" id="addIcon"></img></a>`;
      $('.open-popup-link').magnificPopup({
  type:'inline',
  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});
  }else{
icons.forEach(innerActualiseIcons);
}
}

function innerActualiseIcons(element, index, array) {
document.getElementById("icons").innerHTML+=`<img onclick="openUrl('` + icons[index][0] +`');" class="ab" src="` + icons[index][1] + `">`;
}

function innerActualiseIconsEditmode(element, index, array) {
document.getElementById("icons").innerHTML+=`<a href="#iconOptionsW" class="open-popup-link" onclick="iconOptionsIndex=` + index + `; document.getElementById('iconOptionsW1').value='` + icons[index][0] + `'; document.getElementById('iconOptionsW2').value='` + icons[index][1] + `';"><img class="ab" style="z-index: -2" src="` + icons[index][1] + `"><img src="res/preferences.svg" style="z-index: -1; transform: rotate(90deg); height: 40px; width: 40px; margin-bottom: 32.5px; margin-left: -27.5px;"></img></a>`;
}

function getCookie(cname) {
var name = cname;
var decodedCookie = decodeURIComponent(document.cookie);
var ca = decodedCookie.split(";");
for(var i = 0; i <ca.length; i++) {
c = ca[i];
while (c.charAt(0) == " ") {
            c = c.substring(1);
}
if(c.indexOf(name) == 0) {
    c = c.substring(1);
            return c.substring(name.length, c.length);
        }
}
return;
} 

function getSettings() {
var applySet = getCookie("settings");
if(applySet != null) {
document.getElementById("settingsText").value=getCookie("settings");
settingsArray=JSON.parse(getCookie("settings"));
if(settingsArray!=null){
settings=settingsArray[0];
icons=settingsArray[1];
actualiseIcons();
}
    }
if(settingsArray==null){
    	document.getElementById("settingsText").value=GET("res/PortalSettings.json");
$.getJSON("res/PortalSettings.json", function(data) {
    //console.log(data);
   settingsArray=data;
   settings=settingsArray[0];
icons=settingsArray[1];
actualiseIcons();
});
 }
} 

function setCookie(cname, cvalue, exdays) {
var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = expires + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function saveSettings(){
    settingsArray=["Placeholder"];
    settingsArray[0]=settings;
    settingsArray.push(icons);
setCookie("settings", JSON.stringify(settingsArray), 365);
document.getElementById("settingsText").value=getCookie("settings");
}

function addIcon(){
icons.push([document.getElementById("addIconUrl").value, document.getElementById("addIconIconUrl").value]);
actualiseIcons();
saveSettings();
}

function saveIconOptions(){
if(iconOptionsIndex>(icons.length-1)){
    icons.push([document.getElementById("iconOptionsW1").value, document.getElementById("iconOptionsW2").value]);
    document.getElementById('iconOptionsW1').value="";
    document.getElementById('iconOptionsW2').value="";
}else{
icons[iconOptionsIndex][0]=document.getElementById('iconOptionsW1').value;
icons[iconOptionsIndex][1]=document.getElementById('iconOptionsW2').value;
}
actualiseIcons();
$.magnificPopup.close();
saveSettings();
}

  document.getElementById('import').addEventListener('change', handleFileSelect, false);
window.addEventListener('DOMContentLoaded', getSettings(), false);
